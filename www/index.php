<?php
$args = array();
$args['title'] = 'yoursunny.com';
$args['site_index_title_class'] = 'pure-u-1 pure-u-md-2-5';

$blogContent = @json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/../t/public/content.json')) ?? array();
$blogPosts = array_splice($blogContent, 0, 5);
foreach (array_rand($blogContent, min(count($blogContent), 5)) as $i) {
  $blogPosts[] = $blogContent[$i];
}
$args['blogPosts'] = $blogPosts;

require_once $_SERVER['DOCUMENT_ROOT'].'/_internal/templating.php';
renderTemplate('+'.__COMPILER_HALT_OFFSET__.':'.__FILE__, $args);
__halt_compiler()?>
{% extends "layout.html" %}

{% block head_extra %}
<style>
#page_nav { font-size: 150%; }
#page_nav li a { color: #f012be; }
.page_section h2, .page_section h2 a { color: #111111; }
.page_section li a { color: #0074d9; }
.page_section li a:visited { color: #b10dc9; }
#page_blog a.title { line-height: 200%; }
#page_blog .tag { display: inline-block; padding: 0 0.3em; border-radius: 1em; background: #ffdc00; color: #111111; font-size: 70%; }
#page_about li { line-height: 200%; }
</style>
{% endblock %}

{% block header_right %}
<div class="pure-u-1 pure-u-md-2-5">
<nav id="page_nav" class="pure-menu pure-menu-horizontal" style="margin:1rem;">
<ul class="pure-menu-list">
<li class="pure-menu-item"><a href="/t/" class="pure-menu-link">blog</a>
<li class="pure-menu-item"><a href="/p/" class="pure-menu-link">projects</a>
<li class="pure-menu-item"><a href="/study/" class="pure-menu-link">study</a>
<li class="pure-menu-item"><a href="/m/" class="pure-menu-link">personal</a>
</ul>
</nav>
</div>
{% endblock %}

{% block content %}
<div class="pure-g" style="margin:1rem 0;">
<div class="pure-u-1">
<img class="pure-img" src="/assets/frontpage/20161230101222.jpg" alt="photo of Junxiao Shi">
</div>
</div>

<div class="pure-g">
<section id="page_blog" class="pure-u-1 pure-u-lg-1-2 page_section">
<h2><a href="/t/">Blog Posts</a></h2>
<ul>
{% for post in blogPosts %}
<li><a class="title" href="{{ post.permalink|replace({'https://yoursunny.com/t/':'/t/'}) }}">{{ post.title|e }}</a>
{% for tag in post.tags %}
<a class="tag" href="{{ tag.permalink|replace({'https://yoursunny.com/t/':'/t/'}) }}">{{ tag.name|e }}</a>
{% endfor %}
{% endfor %}
</ul>
</section>
<section id="page_about" class="pure-u-1 pure-u-lg-1-2 page_section">
<h2><a href="/m/">About Me</a></h2>
<ul>
<li><a href="https://twitter.com/yoursunny">@yoursunny</a>
<li><a href="https://pushups.ndn.today/">push-up specialist</a>
<li><a href="https://app.losant.com/dashboards/5a0eb134df167200061adfca">Room temperature and humidity</a>
<li><a href="https://a.co/5M8EPdX">Send me a gift</a>
</ul>
<h2>Goodies</h2>
<ul>
<li><a href="https://bit.ly/huelinvite">Huel: feed for busy life</a>
<li><a href="https://bit.ly/itrefer">Discover It: free $50</a>
<li><a href="https://evolution-host.com/">Hosting by Evolution Host</a>
</ul>
</section>
</div>
{% endblock %}

{% block foot %}
<script>
function resizeNav() {
  document.querySelector('#page_nav').classList
    .toggle('pure-menu-horizontal', window.innerWidth >= 568);
}
window.addEventListener('resize', resizeNav);
resizeNav();
</script>
{% endblock %}
