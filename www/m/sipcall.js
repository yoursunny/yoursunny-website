import { Web } from "https://esm.sh/sip.js@0.21.2";

/** @type {HTMLButtonElement} */
const $callBtn = document.querySelector("#callBtn");
$callBtn.style.display = "none";

let isCalling = false;
const ua = new Web.SimpleUser("wss://edge.sip.onsip.com", {
  media: { remote: { audio: document.querySelector("#sipAudio") } },
});
ua.delegate = {
  onServerConnect() {
    $callBtn.style.display = "";
    $callBtn.disabled = false;
    $callBtn.className = "bg-green";
    $callBtn.textContent = "dial online";
  },
  onServerDisconnect() {
    $callBtn.style.display = "none";
  },
  onCallCreated() {
    $callBtn.disabled = false;
    $callBtn.className = "bg-orange";
    $callBtn.textContent = `VoIP calling | Hangup`;
  },
  onCallAnswered() {
    $callBtn.disabled = false;
    $callBtn.className = "bg-red";
    $callBtn.textContent = `answered | Hang up`;
  },
  onCallHangup() {
    $callBtn.disabled = false;
    $callBtn.className = "bg-green";
    $callBtn.textContent = "call online";
    isCalling = false;
  },
};
ua.connect();

$callBtn.addEventListener("click", () => {
  if (isCalling) {
    ua.hangup();
    return;
  }
  $callBtn.disabled = true;
  ua.call("sip:web@yoursunny.onsip.com");
  isCalling = true;
});
