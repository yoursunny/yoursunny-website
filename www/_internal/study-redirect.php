<?php
function redirect($u) {
  header('Location: '.$u, TRUE, 301);
  die;
}

$uri = substr($_SERVER['REQUEST_URI'], 6); // strip '/study'
$courseid = substr($uri, 1, 5);

if (isset($_GET['p'])) {
  $p = $_GET['p'];
  switch ($courseid) {
    case 'EI103':
      redirect('/study/'.$courseid.'/'.$courseid.'-'.$p.'.pdf');
    case 'IS206':
    case 'IS222':
    case 'IS401':
    case 'IS406':
      redirect('/study/'.$courseid.'/'.$p.'.pdf');
    case 'IS219':
    case 'IS220':
      redirect('/study/'.$courseid.'/p'.$p.'.htm');
    case 'MU019':
      if ($p == 'teaching' || $p == 'index') {
        redirect('/study/MU019/');
      }
      // fallthrough
    default:
      redirect('/study/'.$courseid.'/'.$p.'.htm');
  }
}

if (strncasecmp($uri, '/GPA', 4) == 0) {
  redirect('/p/GPA/');
}

switch ($courseid) {
  case 'EI206':
    redirect('/t/2007/elevator/');
  case 'IS210':
    redirect('/t/2008/underwater-communication/');
  case 'IS496':
    redirect('/t/2007/iptables/');
}

if (isset($_GET['topic']) && ($courseid == 'EI209' || $courseid == 'IS221')) {
  if ($_GET['topic'] == 'index') {
    redirect('/study/'.$courseid.'/');
  }
  else {
    redirect('/study/'.$courseid.'/'.$_GET['topic'].'.htm');
  }
}

if ($courseid == 'IS216') {
  if (isset($_GET['question'])) {
    if ($_GET['question'] == 'list') {
      redirect('/study/IS216/');
    }
    else {
      redirect('/study/IS216/'.$_GET['question'].'.htm');
    }
  }
  if (strncmp($uri, '/IS216/q/', 9) == 0) {
    redirect('/study/IS216/'.substr($uri, 9));
  }
}

if (isset($_GET['w']) && $courseid == 'IS208') {
  $w = $_GET['w'];
  if (strlen($w) == 2 && $w[0] == 'r') {
    redirect('/study/IS209/0'.$w[1].'.htm');
  }
}

switch ($uri) {
  case '/IS206/nachos.htm':
    redirect('/study/IS206/nachos-diary.pdf');
  case '/IS206/nachos1.htm':
    redirect('/study/IS206/nachos-scheduler.pdf');
  case '/IS206/nachos2.htm':
    redirect('/study/IS206/nachos-vm.pdf');
  case '/IS222/outline.htm':
    redirect('/study/IS222/outline.pdf');
  case '/IS222/w1.htm':
    redirect('/study/IS222/game-console.pdf');
  case '/IS222/w2.htm':
    redirect('/study/IS222/linux-arm.pdf');
}

if (strncmp($uri, '/chemistry/?', 12) == 0) {
  if (isset($_GET['nb'])) {
    redirect('/study/chemistry/notebook/#p='.$_GET['nb']);
  }
  if (isset($_GET['organic-experiment'])) {
    if ($_GET['organic-experiment'] == 'index') {
      redirect('/study/chemistry/organic-experiment/');
    }
    else {
      redirect('/study/chemistry/organic-experiment/'.$_GET['organic-experiment'].'.htm');
    }
  }
}

http_response_code(404);
?>
