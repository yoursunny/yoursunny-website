<?php
function redirect($u) {
  header('Location: '.$u, TRUE, 301);
  die;
}

http_response_code(410);

$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$comps = explode('/', $path);
if (count($comps) < 3) {
  die;
}
switch ($comps[2]) {
  case '':
    redirect('/p/');
  case 'AlbumPlayer':
    redirect('/t/2007/AlbumPlayer/');
  case 'ATM':
    redirect('/study/EE305/');
  case 'dbMySQL':
    redirect('/t/2008/dbMySQL/');
  case 'hProxyN':
    redirect('/study/IS494/');
  case 'ImageResizer':
    redirect('/t/2007/ImageResizer/');
  case 'PHP-decode':
    redirect('/p/PHP-decode/');
}
?>
