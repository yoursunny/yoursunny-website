<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

$file = $_GET['f'];
$file = $_SERVER['DOCUMENT_ROOT'].dirname($file).'/'.basename($file);

$content = @file_get_contents($file);
if ($content === FALSE) {
  http_response_code(404);
  die;
}
$doc = KzykHys\FrontMatter\FrontMatter::parse($content);

if (isset($doc['http_status'])) {
  http_response_code(intval($doc['http_status']));
}

$html = '';
if ($_GET['t'] == 'html') {
  $html = $doc->getContent();
}
elseif ($_GET['t'] == 'markdown') {
  $html = Michelf\MarkdownExtra::defaultTransform($doc->getContent());
}
$html = preg_replace('/<table>/i', '<table class="pure-table pure-table-bordered">', $html);

$args = $doc->getConfig();
$args['body'] = $html;

require_once 'templating.php';
renderTemplate(($doc['layout'] ?? 'article').'.html', $args);
?>
