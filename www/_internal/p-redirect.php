<?php
function redirect($u) {
  header('Location: '.$u, TRUE, 301);
  die;
}

http_response_code(404);

$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$comps = explode('/', $path);
if (count($comps) < 3) {
  die;
}
switch ($comps[2]) {
  case 'homecam':
    redirect('https://homecam.ndn.today/');
  case 'ndn-cxx-breaks':
    redirect('https://ndn-cxx-breaks.ndn.today/');
  case 'ndn6':
    redirect('https://github.com/yoursunny/ndn6-tools');
  case 'NDNSF':
    redirect('https://github.com/yoursunny/NDNSF');
  case 'NFD-Windows':
    redirect('/t/2018/NFD-on-Windows-10-WSL/');
  case 'qqwry':
    redirect('/t/2011/qqwry/');
}
?>
