<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

// Load template from end of PHP file
// 1. put __halt_compiler()? > at end of PHP code, and put template after that
// 2. use '+'.__COMPILER_HALT_OFFSET__.':'.__FILE__ as template name
class TwigHaltOffsetLoader implements \Twig\Loader\LoaderInterface
{
  private function parseName(string $name) {
    list($offset, $file) = explode(':', $name);
    if ($offset == '' || $offset[0] != '+' || $file == '') {
      throw new \Twig\Error\LoaderError('TwigHaltOffsetLoader: offset not found');
    }

    $offset = intval($offset);
    return array($offset, $file);
  }

  public function getSourceContext(string $name): \Twig\Source {
    list($offset, $file) = $this->parseName($name);
    $source = substr(file_get_contents($file), $offset);
    return new \Twig\Source($source, $name);
  }

  public function getCacheKey(string $name): string {
    return $name;
  }

  public function isFresh(string $name, int $time): bool {
    list($offset, $file) = $this->parseName($name);
    return filemtime($file) < $time;
  }

  public function exists($name) {
    try {
      $this->parseName($name);
      return true;
    }
    catch (\Twig\Error\LoaderError $e) {
      return false;
    }
  }
}

function renderTemplate($template, $args) {
  $fs = new \Twig\Loader\FilesystemLoader(array('.', $_SERVER['DOCUMENT_ROOT'].'/_layouts'));
  $haltoffset = new TwigHaltOffsetLoader;
  $loader = new \Twig\Loader\ChainLoader(array($fs, $haltoffset));

  $twig = new \Twig\Environment($loader, array('autoescape'=>false, 'auto_reload'=>true, 'cache'=>'/run/shm/yoursunny2017-cache-twig'));
  if (function_exists('extendTwig')) {
    extendTwig($twig);
  }

  $output = $twig->render($template, $args);
  header('Content-Length: '.strlen($output));
  echo $output;
}
?>
