(($ul) => {
  if ($ul?.tagName !== "UL") {
    return;
  }
  const items = Array.from($ul.children, ($li) => [$li, Math.random()]);
  items.sort(([, a], [, b]) => a - b);
  for (const [$li] of items) {
    $li.remove();
    $ul.append($li)
  }
})(document.querySelector("#sponsors + ul"));
