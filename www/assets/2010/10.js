//Copyright 2006-2010 yoursunny.com

/* ---- common script ---- */

function c(k){//send specific url to Google Analytics
	// deleted in 2023 during GA4 upgrade
}

var W={
	resolveURL:function(u){
		return u;
	},
	loadjs:function(u,cb){//load a script
		var s=document.createElement('script');
		s.type='text/javascript';
		s.src=W.resolveURL(u);
		s.async=true;
		var head=document.getElementsByTagName('head')[0];
		var done=false;
		s.onload=s.onreadystatechange=function(){
			if (!done && (!this.readyState || this.readyState=='loaded' || this.readyState=='complete')) {
				done=true;
				if (cb) cb();
				s.onload=s.onreadystatechange=null;
				head.removeChild(s);
			}
		};
		head.appendChild(s);
	},
	load_jquery:function(cb){
		W.loadjs('https://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js',function(){
			W.ready=function(cb){$(cb);};
			if(cb) cb();
		});
		W.load_jquery=function(cb){ if(cb) cb(); };
	},
	bind:function(obj,evt,cb){
		if (typeof cb!='function') return;
		if (obj.addEventListener) {
			obj.addEventListener(evt,cb,false);
		} else if (obj.attachEvent) {
			obj.attachEvent('on'+evt,cb);
		} else {
			var ocb=obj['on'+evt];
			obj['on'+evt]=function(){
				if (typeof ocb=='function') ocb();
				cb();
			};
		}
	},
	ready:function(cb){
		var done=false;
		W.bind(window,'DOMContentLoaded',function(){ if(!done){done=true;cb();} });
		W.bind(window,'load',function(){ if(!done){done=true;cb();} });
	},
	getTagClass:function(tag,className){//$('#tag.className')
		var ELEs=document.getElementsByTagName(tag),
			a=[],cls=' '+className+' ';
		for (var i=0;i<ELEs.length;++i) {
			if ((' '+ELEs[i].className+' ').indexOf(cls)>=0) a.push(ELEs[i]);
		}
		return a;
	},
	prettify:function(len){//google-code-prettify with break-line support
	},
	trackout:function(u){
		if (!/^http/.test(u) || /(?:yoursunny\.com)|(?:\.sunny)/.test(u)) return;
		c('/out/'+u.replace(/https?\:\/\//,''));
	}
};
W.ready(function(){
	W.ready=function(cb){cb();};
});

/* ---- page-specific script ---- */

/* blogger page */
if (/blogspot/.test(location.host)) {
	W.ready(function(){
		var note=/sunnypower/.test(location.host)?' 作者：<a href="http://yoursunny.cn/m/">阳光男孩</a>':' 本文来源网络，著作权归原作者所有';
		var footers=W.getTagClass('p','BloggerPostFooter');
		for (var i=0;i<footers.length;++i) {
			var P=footers[i];
			var A=P.getElementsByTagName('a')[0];
			A.innerHTML=(function(s)
				{
					var dt=new Date(s);
					var d2=function(n){return (n>9?'':'0')+n;};
					return [dt.getFullYear(),'-',d2(dt.getMonth()+1),'-',d2(dt.getDate()),' ',d2(dt.getHours()),':',dt.getMinutes()].join('');
				})(A.innerHTML);
			P.innerHTML+=note;
		}
	});
}
