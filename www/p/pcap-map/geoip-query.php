<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
use GeoIp2\Database\Reader;

$reader = new Reader($_SERVER['DOCUMENT_ROOT'].'/_internal/geoip/GeoLite2-City.mmdb');

$r = array();

foreach (file('php://input') as $line) {
  $ip = trim($line);
  $record = NULL;
  try {
    $record = $reader->city($ip);
  }
  catch (GeoIp2\Exception\AddressNotFoundException $ex) {
  }

  $r[$ip] = @array('country'=>$record->country->name,
                   'city'=>$record->city->name,
                   'lat'=>$record->location->latitude,
                   'lon'=>$record->location->longitude);
}

header('Content/Type: application/json');
echo json_encode($r);
?>
