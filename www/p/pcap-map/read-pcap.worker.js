function FileStreamReader(file) {
  this.m_chunkSize = 1048576;

  this.m_file = file;
  this.m_fileSize = file.size;

  this.m_offset = -this.m_chunkSize; // absolute offset of m_chunk
  this.m_chunk = new ArrayBuffer(0);
  this.m_consumed = 0; // octets consumed in m_chunk
}

// (PUBLIC) read next count octets as Uint8Array
// return: DataView of count unread bytes
FileStreamReader.prototype.read = function(count) {
  var nOctets = Math.min(count, this.m_fileSize - (this.m_offset + this.m_consumed));
  var arr = new Uint8Array(nOctets);
  var nOctetsFromLastChunk = 0;

  if (this.m_offset >= 0) {
    nOctetsFromLastChunk = Math.min(nOctets, this.m_chunk.byteLength - this.m_consumed);
    var octetsFromLastChunk = new Uint8Array(this.m_chunk, this.m_consumed, nOctetsFromLastChunk);
    this.m_consumed += nOctetsFromLastChunk;
    arr.set(octetsFromLastChunk, 0);
  }

  var nOctetsFromNextChunk = nOctets - nOctetsFromLastChunk;
  if (nOctetsFromNextChunk > 0) {
    this.readNextChunk();
    var octetsFromNextChunk = new Uint8Array(this.m_chunk, this.m_consumed, nOctetsFromNextChunk);
    this.m_consumed += nOctetsFromNextChunk;
    arr.set(octetsFromNextChunk, nOctetsFromLastChunk);
  }

  return arr.buffer;
};

// read next chunk
FileStreamReader.prototype.readNextChunk = function() {
  this.m_offset += this.m_chunkSize;
  var blob = this.m_file.slice(this.m_offset, this.m_offset + this.m_chunkSize);
  this.m_chunk = new FileReaderSync().readAsArrayBuffer(blob);
  this.m_consumed = 0;
};

function PcapParser(file) {
  this.m_fsr = new FileStreamReader(file);
  this.m_littleEndian = false;
}

// (PUBLIC) read Global Header
// return {dlt}
// return false on error
PcapParser.prototype.readGlobalHeader = function() {
  var buffer = this.m_fsr.read(24);
  if (buffer.byteLength < 24) {
    return false;
  }
  var gh = new DataView(buffer);

  var magic = gh.getUint32(0);
  if (magic == 0xa1b2c3d4) {
    this.m_littleEndian = false;
  }
  else if (magic == 0xd4c3b2a1) {
    this.m_littleEndian = true;
  }
  else {
    console.log('Global Header: unexpected magic ' + magic);
    return false;
  }

  var major = gh.getUint16(4, this.m_littleEndian);
  var minor = gh.getUint16(6, this.m_littleEndian);
  if (major != 2 || minor != 4) {
    console.log('Global Header: unexpected version ' + major + '.' + minor);
    return false;
  }

  var dlt = gh.getUint32(20, this.m_littleEndian);
  return {dlt:dlt};
};

// (PUBLIC) read Record Header
// return {timestamp, inclLen, origLen}
PcapParser.prototype.readRecordHeader = function() {
  var buffer = this.m_fsr.read(16);
  if (buffer.byteLength < 16) {
    return false;
  }
  var rh = new DataView(buffer);

  return {
    timestamp: rh.getUint32(0, this.m_littleEndian) +
               0.000001 * rh.getUint32(4, this.m_littleEndian),
    inclLen: rh.getUint32(8, this.m_littleEndian),
    origLen: rh.getUint32(12, this.m_littleEndian)
  };
};

// (PUBLIC) read packet
// param rh: Record Header
// return packet octets as ArrayBuffer
PcapParser.prototype.readPacket = function(rh) {
  var buffer = this.m_fsr.read(rh.inclLen);
  if (buffer.byteLength < rh.inclLen) {
    return false;
  }
  return buffer;
};

// parse Ethernet header
// return {ethtype, size}
// return false on error
function parseEthernet(pkt) {
  if (pkt.byteLength < 14) {
    return false;
  }

  var eh = new DataView(pkt);
  return {
    size: 14,
    ethtype: eh.getUint16(12)
  };
}

// parse Linux Cooked Capture header
// return {ethtype, size}
// return false on error
function parseSll(pkt) {
  if (pkt.byteLength < 16) {
    return false;
  }

  var sllh = new DataView(pkt);
  return {
    size: 16,
    ethtype: sllh.getUint16(14)
  };
}

// parse IPv4 header
// return {src, dst, proto, size}
// return false on error
function parseIp4(pkt, startOffset) {
  var iph = new DataView(pkt, startOffset);
  if (iph.byteLength < 20) {
    return false;
  }
  if ((iph.getUint8(0) & 0xf0) != 0x40) {
    return false;
  }

  return {
    size: 4 * (iph.getUint8(0) & 0x0f),
    src: iph.getUint32(12),
    dst: iph.getUint32(16),
    proto: iph.getUint8(9)
  };
}

function formatIp4(ip) {
  var ab = new ArrayBuffer(4);
  var dv = new DataView(ab);
  dv.setUint32(0, ip);
  return [].join.call(new Uint8Array(ab), '.');
}

// parse TCP header
// return {srcPort, dstPort, size}
function parseTcp(pkt, startOffset) {
  var tcph = new DataView(pkt, startOffset);
  if (tcph.byteLength < 20) {
    return false;
  }

  return {
    size: 4 * (tcph.getUint8(8) >> 4),
    srcPort: tcph.getUint16(0),
    dstPort: tcph.getUint16(2)
  };
}

// parse UDP header
// return {srcPort, dstPort, size}
function parseUdp(pkt, startOffset) {
  var udph = new DataView(pkt, startOffset);
  if (udph.byteLength < 8) {
    return false;
  }

  return {
    size: 8,
    srcPort: udph.getUint16(0),
    dstPort: udph.getUint16(2)
  };
}

// extract five-tuple from packet
// return {proto, src, srcPort, dst, dstPort}
// return false on error
function extractFiveTuple(pkt, parseLinkHeader) {
  var eh = parseLinkHeader(pkt);
  if (eh === false || eh.ethtype != 0x0800) {
    return false;
  }

  var iph = parseIp4(pkt, eh.size);
  if (iph === false) {
    return false;
  }

  switch (iph.proto) {
  case 6:
    var tcph = parseTcp(pkt, eh.size + iph.size);
    if (tcph === false) {
      return false;
    }
    return {proto:iph.proto, src:formatIp4(iph.src), srcPort:tcph.srcPort,
                             dst:formatIp4(iph.dst), dstPort:tcph.dstPort};
  case 17:
    var udph = parseUdp(pkt, eh.size + iph.size);
    if (udph === false) {
      return false;
    }
    return {proto:iph.proto, src:formatIp4(iph.src), srcPort:udph.srcPort,
                             dst:formatIp4(iph.dst), dstPort:udph.dstPort};
  default:
    return false;
  }
}

self.onmessage = function(e) {
  var file = e.data;
  var pp = new PcapParser(file);
  var gh = pp.readGlobalHeader();
  if (gh === false) {
    postMessage({err:'pcap Global Header error'});
    return;
  }
  var parseLinkHeader = false;
  switch (gh.dlt) {
  case 1:
    parseLinkHeader = parseEthernet;
    break;
  case 113:
    parseLinkHeader = parseSll;
    break;
  default:
    postMessage({err:'unknown DLT'});
    break;
  }

  var tables = {
    http:{},
    https:{},
    otherTcp:{},
    dns:{},
    otherUdp:{}
  };

  var rh;
  while (false !== (rh = pp.readRecordHeader())) {
    var pkt = pp.readPacket(rh);
    if (pkt === false) {
      break;
    }
    var ft = extractFiveTuple(pkt, parseLinkHeader);
    if (ft === false) {
      continue;
    }

    var tableId = false;
    switch (ft.proto) {
    case 6:
      switch (ft.srcPort) {
      case 80:
        tableId = 'http';
        break;
      case 443:
        tableId = 'https';
        break;
      default:
        tableId = 'otherTcp';
        break;
      }
      break;
    case 17:
      switch (ft.srcPort) {
      case 53:
        tableId = 'dns';
        break;
      default:
        tableId = 'otherUdp';
        break;
      }
      break;
    }
    if (tableId !== false) {
      tables[tableId][ft.src] = tables[tableId][ft.src] || 0;
      ++tables[tableId][ft.src];
    }
  }

  postMessage(tables);
};
