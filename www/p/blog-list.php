<?php
$rtag = @$_GET["tag"];

$args = array();
$args['tag'] = $rtag;

$blogContent = @json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'].'/../t/public/content.json')) ?? array();
$blogPosts = array_filter($blogContent, function($v) {
  global $rtag;
  foreach (($v->tags ?? array()) as $tag) {
    if ($tag->name === $rtag) {
      return TRUE;
    }
  }
  return FALSE;
});
$args['blogPosts'] = $blogPosts;

header('Content-Type: text/javascript');
require_once $_SERVER['DOCUMENT_ROOT'].'/_internal/templating.php';
renderTemplate('+'.__COMPILER_HALT_OFFSET__.':'.__FILE__, $args);
__halt_compiler()?>
document.querySelector('script.blog-list-{{ tag|e('js') }}').outerHTML = `
<ul class="blog-list">
{% for post in blogPosts %}
<li><a class="title" href="{{ post.permalink|replace({'https://yoursunny.com/t/':'/t/'}) }}">{{ post.title|e }}</a>
<time>{{ post.date|e }}</time>
{% endfor %}
</ul>
`;
