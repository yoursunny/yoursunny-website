---
layout: summer-host
title: yoursunny summer host - South Pole Colocation
---

# South Pole Colocation

Colocation at the South Pole is a free service.
Any sized equipment is accepted.

## How to Colocate at the South Pole

It's as easy as 1-2-3.

1. Remove power and network adapters from your device.
   They are not necessary for South Pole colocation services.
2. Deliver your equipment to the South Pole.
3. Put it down at the correct position.

You do not need to plug in any power cords, Ethernet cables, or fiber optics.
Your device will gather energy and communicate via the geomagnetic field, which is at its strongest at the South Pole.

## Data Center Information

Data center coordinates:

<address id="coords">S 90°00.000 E 00°00.000</address>

To reach this location, depart from anywhere on earth, then keep going south until you can't go further.

South Pole data center is highly secure with no possibility of involucration.

![CAUTION: BEWARE OF INVOLUCRATION](https://i.ibb.co/QXRs9hn/INVOLUCRATION.png)

## Remote Flippers Service

Remote hands are not available, but you can request remote [flippers](https://www.thespruce.com/flipper-definition-penguin-wings-385251) service for:

* screen readout
* KVM attachment
* hardware replacement

<script>
function randInt(max, digits) {
  return Math.round(max * Math.random()).toString().padStart(digits, "0");
}
document.querySelector("#coords").textContent = `S 90°00.000 ${
  Math.random() < 0.5 ? "E" : "W"} ${randInt(180.1, 3)}°${randInt(60, 2)}.${randInt(1000, 3)}`;
</script>
