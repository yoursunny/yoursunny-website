/**
 * Count of 1 bits in each byte 0x00..0xFF.
 * @type {number[]}
 */
const ONES = [];
for (let b = 0x00; b <= 0xFF; ++b) {
  let cnt = 0;
  for (let s = 0; s < 8; ++s) {
    if ((b >> s) % 2 === 1) {
      ++cnt;
    }
  }
  ONES.push(cnt);
}

/**
 * Count number of 0 and 1 bits.
 * @param {Blob} blob
 * @returns {Promise<[cnt0: number, cnt1: number]>}
 */
async function countBitsBlob(blob) {
  const { size } = blob;
  const percent = size / 100;
  let cnt = 0;
  const reader = blob.stream().getReader();
  let total = 0;
  let nextReport = total + percent;
  let nextTime = Date.now() + 100;
  while (true) {
    /** @type {{ done: boolean; value: Uint8Array; }} */
    const { done, value: chunk } = await reader.read();
    if (done) {
      break;
    }

    total += chunk.length;
    if (total >= nextReport) {
      const now = Date.now();
      const delayTime = nextTime - now;
      if (delayTime > 0) {
        await new Promise((resolve) => setTimeout(resolve, delayTime));
      }
      nextTime = now + 100;
      postMessage({ type: "progress", progress: total / size });
      nextReport = total + percent;
    }

    for (const b of chunk) {
      cnt += ONES[b];
    }
  }
  return [8 * size - cnt, cnt];
}

/**
 * Message handler.
 * @param {MessageEvent<File>} evt
 */
onmessage = async (evt) => {
  try {
    const result = await countBitsBlob(evt.data);
    postMessage({ type: "result", result });
  } catch (err) {
    postMessage({ type: "error", error: `${err}` });
  }
};
