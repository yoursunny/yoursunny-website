<?php
require_once 'gtfs-db.inc.php';
date_default_timezone_set('America/New_York');
define('GTFS_DBNAME', 'data/RideOnGTFS.sqlite3');

$today = mktime(0, 0, 0);
$date = $today;
$noindex = FALSE;
if (isset($_GET['dt'])) {
  if (preg_match('/^\d{4}\-\d{2}\-\d{2}$/', $_GET['dt'])) {
    $date = strtotime($_GET['dt']);
    $noindex = TRUE;
  } else {
    header('Location: ./');
    die;
  }
}
$datestr = 'on '.date('F j', $date);
if ($date == $today) {
  $datestr = 'today';
} elseif ($date == $today + 86400) {
  $datestr = 'tomorrow';
} elseif ($date > $today && $date <= $today + 86400 * 6) {
  $datestr = 'on '.date('l', $date);
}

$db = new SQLite3(GTFS_DBNAME, SQLITE3_OPEN_READONLY);
if (@$_GET['a'] == 'stops') {
  header('Content-Type: application/json');
  $stops = gtfs_stops_by_date($db, $date);
  echo json_encode($stops);
  die;
}

$routes = gtfs_routes_by_date($db, $date);
$db->close();

$firstIndex = 0;
foreach ($routes as $i=>$route) {
  if ($route['route_id'] != $routes[$firstIndex]['route_id']) {
    $firstIndex = $i;
  }
  $routes[$firstIndex]['headsign_count'] = $i - $firstIndex + 1;
}

$args = array();
$args['title'] = sprintf('Ride On Today: what bus routes are running %s?', $datestr);
$args['section'] = array('title'=>'Projects', 'path'=>'/p/');
$args['datets'] = $date;
$args['noindex'] = $noindex;
$args['past'] = $date < $today;
$args['datestr'] = $datestr;
$args['routes'] = $routes;
require_once $_SERVER['DOCUMENT_ROOT'].'/_internal/templating.php';
define('ALLROUTES', 'https://www.montgomerycountymd.gov/DOT-Transit/routesandschedules/allroutes/');
function extendTwig($twig) {
  $twig->addFilter(new \Twig\TwigFilter('route_uri', function($route_number) {
    return ALLROUTES.match($route_number) {
      'FLASH' => 'flash-orange',
      'L8', 'T2' => sprintf('route%s', strtolower($route_number)),
      default => sprintf('route%03d', $route_number),
    }.'.html';
  }));
}
renderTemplate('+'.__COMPILER_HALT_OFFSET__.':'.__FILE__, $args);
__halt_compiler()?>
{% extends "layout.html" %}

{% block head_extra %}
{% if noindex %}<meta name="robots" content="noindex">{% endif %}
<style>
.hide_triptimes .triptimes { display:none; }
#map_container { height:75vw; }
</style>
{% endblock %}

{% block content %}
<div class="pure-g">
<div class="pure-u-1">
<h1><a href="./">Ride On Today</a>: what bus routes {% if past %}were{% else %}are{% endif %} running {{ datestr }}?</h1>
<p>
{% for off in -1..6 %}
<a class="pure-button {% if off==0 %}pure-button-active{% endif %}" href="./?dt={{ (datets+86400*off)|date('Y-m-d') }}" rel="nofollow">{{ (datets+86400*off)|date('D M d') }}</a>
{% endfor %}
</p>
</div>

<div class="pure-u-1 pure-u-lg-1-3" id="trip_table_container">

<div class="pure-form">
<label for="show_triptimes" class="pure-checkbox"><input id="show_triptimes" type="checkbox" value="">show trip times</label>
</div>

<table id="trip_table" class="pure-table pure-table-horizontal hide_triptimes">
<thead>
<tr>
<th>#
<th>route
<th class="triptimes">direction
<th class="triptimes">trips
<th class="triptimes">first
<th class="triptimes">last
<tbody>
{% for route in routes %}
<tr>
{% if route.headsign_count %}
<td rowspan="{{ route.headsign_count }}"><a href="{{ route.route_number|route_uri }}" target="_blank" rel="external">{{ route.route_number|e }}</a>
<td rowspan="{{ route.headsign_count }}">{{ route.route_name|e }}
{% endif %}
<td class="triptimes">{{ route.trip_headsign|e }}
<td class="triptimes">{{ route.trip_count }}
<td class="triptimes">{{ route.first_departure|e }}
<td class="triptimes">{{ route.last_departure|e }}
{% endfor %}
</table>
</div>

<div class="pure-u-1 pure-u-lg-2-3" id="map_container"></div>

</div>
{% endblock %}

{% block footer_addtext %}
, built with <a href="https://www.montgomerycountymd.gov/DOT-Transit/GTFS/GTFSfiles.html">Ride On GTFS feed</a>
{% endblock %}

{% block foot %}
<script>
document.querySelector('#show_triptimes').addEventListener('change', (evt) => {
  const method = evt.target.checked ? 'remove' : 'add';
  document.querySelector('#trip_table').classList[method]('hide_triptimes');
  document.querySelector('#trip_table_container').classList[method]('pure-u-lg-1-3');
  document.querySelector('#map_container').classList[method]('pure-u-lg-2-3');
});

let map;
function initMap() {
  map = new google.maps.Map(document.querySelector("#map_container"), {
    mapId: "DEMO_MAP_ID",
    center: { lat: 39.154700, lng: -77.240500 },
    zoom: 11,
    mapTypeControl: false,
    streetViewControl: false,
    fullscreenControl: false,
  });
  fetchStops().catch(console.error);
}

async function fetchStops() {
  const resp = await fetch('?dt={{ datets|date('Y-m-d') }}&a=stops');
  if (!resp.ok) {
    throw new Error(`HTTP ${response.status}`);
  }
  const stops = await resp.json();

  const outer = new google.maps.LatLngBounds({ lat: 38, lng: -78 }, { lat: 40, lng: -76 });
  const bounds = new google.maps.LatLngBounds();
  for (const stop of stops) {
    const position = new google.maps.LatLng(parseFloat(stop.stop_lat), parseFloat(stop.stop_lon));
    if (!outer.contains(position)) {
      continue;
    }
    bounds.extend(position);
    const $img = document.createElement('img');
    $img.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAYAAACp8Z5+AAAAE0lEQVR42mNkaPj/nwEJMJIuAACIxQn9ppoUNwAAAABJRU5ErkJggg==';
    new google.maps.marker.AdvancedMarkerElement({ map, position, content: $img });
  }
  map.fitBounds(bounds);
}
</script>
<script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZkmau2JPqTHVshtguOiFwC1MhW9AGEck&loading=async&callback=initMap&libraries=marker"></script>
{% endblock %}
