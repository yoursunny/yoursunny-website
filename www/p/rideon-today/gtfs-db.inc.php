<?php
define('GTFSSQL_SERVICES_BY_DATE', 'SELECT service_id_map.service_id_str AS service_id FROM service_id_map INNER JOIN calendar ON service_id_map.service_id=calendar.service_id WHERE calendar.date=:date');

function gtfs_routes_by_date(SQLite3 $db, int $date): array {
  $stmt = $db->prepare('SELECT routes.route_id,route_number,route_name,trip_headsign,COUNT(*) AS trip_count,MIN(departure_time) AS first_departure,MAX(departure_time) AS last_departure FROM routes INNER JOIN trips ON routes.route_id=trips.route_id INNER JOIN stop_times ON trips.trip_id=stop_times.trip_id WHERE service_id IN ('.GTFSSQL_SERVICES_BY_DATE.') AND stop_sequence=1 GROUP BY routes.route_id,trip_headsign ORDER BY CAST(route_number AS INT)');
  $stmt->bindValue(':date', date('Ymd', $date));
  $result = $stmt->execute();
  $a = array();
  while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
    array_push($a, $row);
  }
  $result->finalize();
  return $a;
}

function gtfs_stops_by_date(SQLite3 $db, int $date): array {
  $stmt = $db->prepare('SELECT DISTINCT stop_code,stop_lat,stop_lon FROM trips INNER JOIN stop_times ON trips.trip_id=stop_times.trip_id INNER JOIN stops ON stop_times.stop_id=stops.stop_id WHERE service_id IN ('.GTFSSQL_SERVICES_BY_DATE.')');
  $stmt->bindValue(':date', date('Ymd', $date));
  $result = $stmt->execute();
  $a = array();
  while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
    array_push($a, $row);
  }
  $result->finalize();
  return $a;
}
?>
