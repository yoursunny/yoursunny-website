#!/bin/bash
set -eo pipefail
FILENAME=${1:-RideOnGTFS}

if [[ -z $DBG ]]; then
  rm -f data/*.*
else
  rm -f data/RideOnGTFS.sqlite3
fi
cd data

if ! [[ -f agency.txt ]]; then
  wget https://www.montgomerycountymd.gov/DOT-Transit/Resources/Files/GTFS/$FILENAME.zip
  unzip $FILENAME.zip
  sed -i '1s/^\xEF\xBB\xBF//' *.txt
  rm $FILENAME.zip
fi
if ! [[ -f calendar.txt ]]; then
  echo 'service_id,monday,tuesday,wednesday,thursday,friday,saturday,sunday,start_date,end_date' > calendar.txt
fi
if ! [[ -f calendar_dates.txt ]]; then
  echo 'service_id,date,exception_type' > calendar_dates.txt
fi

sqlite3 RideOnGTFS.sqlite3 <<EOT
.mode csv

.import calendar.txt calendar_csv
.import calendar_dates.txt calendar_dates_csv
CREATE TABLE service_id_map (
  service_id INT PRIMARY KEY,
  service_id_str TEXT NOT NULL
);
CREATE TABLE calendar (
  date INT NOT NULL,
  service_id INT NOT NULL
);
CREATE INDEX calendar_date ON calendar(date);

.import routes.txt routes_csv
CREATE TABLE routes (
  route_id INT PRIMARY KEY,
  route_number TEXT NOT NULL,
  route_name TEXT NOT NULL
);
EOT

php ../prepare.php

sqlite3 RideOnGTFS.sqlite3 <<EOT
.mode csv
DROP TABLE routes_csv;
DROP TABLE calendar_csv;
DROP TABLE calendar_dates_csv;

.import trips.txt trips
.import stops.txt stops
.import stop_times.txt stop_times

CREATE UNIQUE INDEX stops_stop_id ON stops(stop_id);
CREATE INDEX stops_stop_code ON stops(stop_code);
CREATE UNIQUE INDEX stop_times_trip_id_stop_sequence ON stop_times(trip_id,stop_sequence);
CREATE UNIQUE INDEX trips_trip_id ON trips(trip_id);
CREATE INDEX trips_route_id ON trips(route_id);

VACUUM;
EOT

if [[ -z $DBG ]]; then
  rm *.txt
fi
