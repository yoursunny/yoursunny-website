<?php
php_sapi_name() == 'cli' || die;
date_default_timezone_set('America/New_York');
$db = new SQLite3('RideOnGTFS.sqlite3');

function assignId(&$a, $value) {
  do {
    $id = mt_rand();
  } while (array_key_exists($id, $a));
  $a[$id] = $value;
  return $id;
}

// ---- assign numbers to service_id
$serviceIds = array();
$result = $db->query('SELECT service_id FROM calendar_csv UNION SELECT service_id FROM calendar_dates_csv WHERE exception_type=1');
$stmt1 = $db->prepare('INSERT INTO service_id_map (service_id,service_id_str) VALUES (?,?)');
while ($row = $result->fetchArray(SQLITE3_NUM)) {
  list($serviceIdStr) = $row;
  $stmt1->clear();
  $stmt1->bindValue(1, assignId($serviceIds, $serviceIdStr));
  $stmt1->bindValue(2, $serviceIdStr);
  $stmt1->execute();
}
$stmt1->close();
$result->finalize();
$serviceIdMap = array_flip($serviceIds);

// ---- generate calendar
$row = $db->querySingle('SELECT MIN(start_date) AS minDate, MAX(end_date) AS maxDate FROM calendar_csv', TRUE);
$minDt = DateTime::createFromFormat('Ymd', $row['minDate'])->getTimestamp();
$maxDt = DateTime::createFromFormat('Ymd', $row['maxDate'])->getTimestamp();

$stmt = $db->prepare('SELECT service_id FROM calendar_csv WHERE start_date<=:date AND end_date>=:date AND monday>=(strftime("%w",:dt)=="1") AND tuesday>=(strftime("%w",:dt)=="2") AND wednesday>=(strftime("%w",:dt)=="3") AND thursday>=(strftime("%w",:dt)=="4") AND friday>=(strftime("%w",:dt)=="5") AND saturday>=(strftime("%w",:dt)=="6") AND sunday>=(strftime("%w",:dt)=="0") AND service_id NOT IN (SELECT service_id FROM calendar_dates_csv WHERE date=:date AND exception_type=2) UNION ALL SELECT service_id FROM calendar_dates_csv WHERE date=:date AND exception_type=1');
$stmt1 = $db->prepare('INSERT INTO calendar (date,service_id) VALUES (:date,:service_id)');
for ($dt = $minDt; $dt <= $maxDt; $dt += 86400) {
  $stmt->clear();
  $stmt->bindValue(':date', date('Ymd', $dt));
  $stmt->bindValue(':dt', date('Y-m-d', $dt));
  $result = $stmt->execute();
  while ($row = $result->fetchArray(SQLITE3_NUM)) {
    list($serviceId) = $row;
    $stmt1->clear();
    $stmt1->bindValue(':date', date('Ymd', $dt));
    $stmt1->bindValue(':service_id', $serviceIdMap[$serviceId]);
    $stmt1->execute();
  }
  $result->finalize();
}
$stmt1->close();
$stmt->close();

// ---- generate routes
$result = $db->query('SELECT route_id,route_short_name,route_long_name FROM routes_csv');
$stmt1 = $db->prepare('INSERT INTO routes (route_id,route_number,route_name) VALUES (?,?,?)');
while ($row = $result->fetchArray(SQLITE3_NUM)) {
  list($routeId,$shortName,$longName) = $row;
  $stmt1->clear();
  $stmt1->bindValue(1, intval($routeId));
  $stmt1->bindValue(2, $shortName);
  $routeName = $longName;
  if (substr($routeName, 0, strlen($shortName) + 1) == $shortName.'-') {
    $routeName = substr($routeName, strlen($shortName) + 1);
  }
  $stmt1->bindValue(3, $routeName);
  $stmt1->execute();
}
$stmt1->close();
$result->finalize();
?>
