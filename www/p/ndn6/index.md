---
title: ndn6 Network
section:
  title: Projects
  path: /p/
---

I have too many idle servers so I installed my own global scale NDN network.

* [network map](map/)
* [routing status](https://nlsr-status.ndn.today/#network=yoursunny)
* [software](https://github.com/yoursunny/ndn6-tools)

## Connecting to ndn6 Network

My network interconnects with the [global NDN testbed](https://named-data.github.io/testbed/).
You can reach my network by using forwarding hint `/yoursunny`.

Some routers can accept HTTP/3 connections from [NDNts](/p/NDNts/) web applications.
You can discover them via [NDN-FCH 2021](https://github.com/11th-ndn-hackathon/ndn-fch) API.

## Prefix Registration Policy

ndn6 network accepts NFD-style prefix registration commands under `/localhop/nfd`.
You should set *Origin=65* to readvertise the prefix within ndn6 network.

Each command must be signed with a certificate issued by either yoursunny authority or [named-data.net authority](https://named-data.net/ndn-testbed/user-guide-to-obtain-a-testbed-certificate/).
The end host must be able to serve every certificate in the certificate chain (excluding the root certificate).

Each prefix must start with either the identity name of your certificate or one of these multicast prefixes: `/ndn/broadcast`, `/ndn/multicast`, `/yoursunny/M`.
Multicast prefixes are not automatically added, and the end host will not receive multicast Interests without a prefix registration.

## Blog Articles

<script class="blog-list-ndn6" src="../blog-list.php?tag=ndn6" async></script>

## Sponsors

I'd like to thank the following sponsors for providing free compute and network resources to the ndn6 network:

<div id="sponsors"></div>

* [Cloudie](https://my.cloudie.sh/index.php?rp=/store/toronto-ca-ixpbgp-vps): Toronto, Canada
* [EU.org](https://nic.eu.org/): network domain
* [Evolution Host](https://evolution-host.com/): Roubaix, France
* [Hosteroid](https://www.hosteroid.uk/): Bucharest, Romania
* [NanoKVM](https://nanokvm.net/): Liberty Lake, Washington, USA (monitoring node)

<script src="/assets/2017/shuffle-sponsors.js"></script>
