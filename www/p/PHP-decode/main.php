<?php

function phpdecode_main($code) {
	//微盾
	if (phpdecode_detect($code,array('__FILE__',"GLOBALS['OOO0000O0']",'eval(base64_decode(','IIIIII'))) {
		if (phpdecode_attempt($code,'vidun')) return $code;
	}
	return FALSE;
}

function phpdecode_detect($code,$signatures) {
	foreach ($signatures as $signature) {
		if (strpos($code,$signature)!==FALSE) return TRUE;
	}
	return FALSE;
}

function phpdecode_attempt(&$code,$lib) {
	include_once $lib.'.php';
	$libmain=$lib.'_main';
	$code1=$libmain($code);
	if ($code1===NULL || $code1===FALSE || $code1==$code) return FALSE;
	else {
		$code=$code1;
		return TRUE;
	}
}

?>
