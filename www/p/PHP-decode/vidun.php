<?php
//解码：微盾PHP脚本加密专家
//Copyright 2011 yoursunny.com , CreativeCommons BY-SA 3.0

function vidun_main($code) {
	$lines=explode("\n",$code);
	if (count($lines)==3
		&& strpos($lines[1],'__FILE__')!==FALSE && strpos($lines[1],'urldecode')!==FALSE
		&& strpos($lines[2],'?')===FALSE) {//Enable Encoding
		$code=vidun_remove_encoding($lines);
	}
	if (strpos($code,"GLOBALS['OOO0000O0']")!==FALSE) {//Enable Obfuscation - Replace Strings
		$code=vidun_unobf_strings($code);
	}
	if (preg_match("/^eval\(base64_decode\('([^']+)'\){2};/",$lines[1],$match)) {//Enable Obfuscation - Replace Standard Functions
		$functions=vidun_extract_functions(base64_decode($match[1]));
		$code=vidun_unobf_functions($code,$functions);
	}
	if (preg_match("/[Il1]{12}/",$code)) {//Enable Obfuscation - Function / Variable Name Replacement
		$namelist=vidun_extract_identifiers($code);
		$names=vidun_identifier_names($namelist);
		$code=vidun_rename_identifiers($code,$names);
	}
	return $code;
}

function vidun_remove_encoding($lines) {
	if (!preg_match("/\('([^']+)'\){3}/",$lines[1],$match)) return '//vidun_remove_encoding failure 1';
	$c1=base64_decode($match[1]);
	if (!preg_match("/,(\d+)\),'([^']+)','([^']+)'\){3}/",$c1,$match)) return '//vidun_remove_encoding failure 2';
	$c2_size=$match[1];
	$strtr_from=$match[2];
	$strtr_to=$match[3];
	$c2=substr($lines[2],$c2_size);
	$c3=strtr($c2,$strtr_from,$strtr_to);
	return base64_decode($c3);
}

function vidun_unobf_strings_callback($match) {
	return var_export(base64_decode($match[1]),TRUE);
}
function vidun_unobf_strings($code) {//取消字符串混淆
	return preg_replace_callback('/\$'."GLOBALS\['OOO0000O0'\]\('([^']*)'\)/",'vidun_unobf_strings_callback',$code);
}

function vidun_extract_functions($definition) {
	$a=array();
	preg_match_all('/\$'."([Il1]+)='([a-z\_0-9]+)';/",$definition,$matches,PREG_SET_ORDER);
	foreach ($matches as $match) {
		$a[$match[1]]=$match[2];
	}
	return $a;
}
function vidun_unobf_functions($code,$functions) {//取消内置函数混淆
	foreach ($functions as $name_obf=>$name) {
		$code=str_replace('$'."GLOBALS['".$name_obf."']",$name,$code);
	}
	return $code;
}

function vidun_extract_identifiers($code) {//收集12字符Il1标志名
	$a=array();
	preg_match_all('/[Il1]{12}/',$code,$matches,PREG_SET_ORDER);
	foreach ($matches as $match) {
		$a[]=$match[0];
	}
	return array_unique($a);
}
function vidun_Il1_name2($v2) {//获取2字符Il1子串对应的简短标志名
	return strval(strpos('Il1',$v2[0])*3+strpos('Il1',$v2[1]));
}
function vidun_Il1_name($v) {//获取12字符Il1串对应的简短标志名
	if (strlen($v)!=12) return $v;
	$n='';
	for ($i=0;$i<12;$i+=2) {
		$n.=vidun_Il1_name2(substr($v,$i,2));
	}
	return $n;
}
function vidun_identifier_names($namelist) {//创建新变量名
	$a=array();
	foreach ($namelist as $name) {
		$a[$name]='v'.vidun_Il1_name($name);
	}
	return $a;
}
function vidun_rename_identifiers($code,$names) {//替换Il1组成的变量名成易读的名称
	foreach ($names as $name_obf=>$name) {
		$code=str_replace($name_obf,$name,$code);
	}
	return $code;
}

?>
