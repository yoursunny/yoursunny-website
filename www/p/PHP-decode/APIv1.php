<?php
include 'main.php';

if ($_SERVER['REQUEST_METHOD']!='POST') {
	header('HTTP/1.1 405 Method Not Allowed');
	header('Content-Type: text/plain');
	die;
}

$code=file_get_contents('php://input');
$result=phpdecode_main($code);

if ($result===FALSE) {
	header('HTTP/1.1 400 DECODE FAILURE');
	header('Content-Type: text/plain');
	die('cannot decode this script');
}

define('GUESS_ENCODING_GUID','5b2c0374-0cca-481b-8eee-089f172d19f6');
$f_iconv=@iconv('gbk','utf-8',$result.GUESS_ENCODING_GUID);
if ($f_iconv!==FALSE && strrpos($f_iconv,GUESS_ENCODING_GUID)!==FALSE) {//file was GBK because transform succeeded and nothing truncated
	$result=substr($f_iconv,0,strlen($f_iconv)-strlen(GUESS_ENCODING_GUID));
}

header('Content-Type: application/x-httpd-php');
echo "<"."?php\r\n".$result."\r\n?".">";

?>
