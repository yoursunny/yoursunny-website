---
http_status: 410
title: Sun Tran SMS Schedule
section:
  title: Projects
  path: /p/
---

Sun Tran SMS Schedule was a tool to query Tucson AZ bus schedule through text messaging, developed in Mar 2012.
Since then, most free text messaging platforms (such as Smsified and Bot.im) have been closed.
Due to the unavailability of a free text messaging API, Sun Tran SMS Schedule is closed in Dec 2015.
