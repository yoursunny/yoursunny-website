GPA.tools={
	init:function(){
		GPA.menu.registerAction('tools',GPA.tools.prepareMenu);
		GPA.menu.registerAction('tools_chart',GPA.chart.start);
		GPA.menu.registerAction('tools_bygroup',function(){GPA.bygroup.start('menu-tools');});
		$('#result_bygroup').click(function(){GPA.bygroup.start('inputui-bottom');});
	},
	prepareMenu:function(){
		var hasCourse=GPA.courses.hasCourse();
		GPA.menu.toggleEnabled('tools_chart',hasCourse);
	}
};

GPA.chart={
	start:function(){
		GPA.stat('tools/chart');
		GPA.popup.open('绩点分布图','<p><img src="'+GPA.chart.buildChartURI(GPA.chart.collectByPoint())+'" alt="绩点分布图"></p>');
	},
	collectByPoint:function(){
		var r={};
		GPA.courses.each(function(id,C){
			var point=GPA.util.fix1(C.point);
			if (r[point]) ++r[point];
			else r[point]=1;
		},true);
		var a=[];
		$.each(r,function(point,count){
			point=parseFloat(point);
			var p=[point,count];
			var i=0;
			while (i<a.length && a[i][0]<point) ++i;
			a.splice(i,0,p);
		});
		return a;
	},
	buildChartURI:function(a){
		var chd=[],chxl=[],count_max=0;
		$.each(a,function(i,p){
			chd.push(p[1]);
			chxl.push(p[0]);
			count_max=Math.max(count_max,p[1]);
		});
		++count_max;
		return 'http://chart.apis.google.com/chart?chs=480x200&cht=bvs&chd=t:'+chd.join(',')+'&chds=0,'+count_max+'&chxt=x,y&chxl=0:|'+chxl.join('|')+'&chxr=1,0,'+count_max+','+Math.floor(count_max/4)+'&chm=N,000000,0,-1,11';
	}
};

GPA.bygroup={
	start:function(medium){
		GPA.stat('tools/bygroup?medium='+medium);
		var allGroups=GPA.bygroup.collectAllGroups();
		var TABLE=$('<table id="report_bygroup"><thead><tr><td>组代码</td><td>课程数</td><td>总学分</td><td>GPA</td></thead><tbody></tbody></tr>');
		var TBODY=TABLE.find('tbody');
		for (var i=0;i<allGroups.length;++i) {
			var g=allGroups.charAt(i);
			var R=GPA.bygroup.calcGroup(g);
			var TR=$('<tr>');
			$('<td>').text(g).appendTo(TR);
			$('<td>').text(R.course_count).appendTo(TR);
			$('<td>').text(R.credit_total).appendTo(TR);
			var v_GPA;
			if (R.course_count==0 || R.credit_total==0) v_GPA='—';
			else if (isNaN(R.GPA)) v_GPA='×';
			else v_GPA=GPA.util.fix4(R.GPA);
			$('<td>').text(v_GPA).appendTo(TR);
			TBODY.append(TR);
		}
		GPA.popup.open('分组统计结果',TABLE);
	},
	collectAllGroups:function(){
		var r='';
		GPA.courses.each(function(id,C){
			for (var i=0;i<C.groups.length;++i) {
				var g=C.groups.charAt(i);
				if (r.indexOf(g)<0) r+=g;
			}
		});
		return r;
	},
	buildFilter:function(g){
		return function(id,C){ return C.selected && C.groups.indexOf(g)>=0; };
	},
	calcGroup:function(g){
		var collector=new GPA.collector(GPA.bygroup.buildFilter(g));
		return collector.getResult();
	},
	resultToString:function(R){
		if (R.course_count==0 || R.credit_total==0) {
			return '没有选中的课程';
		} else {
			if (isNaN(R.GPA)) {
				return '部分成绩无法识别，未能算出结果';
			} else {
				return ''+R.course_count+'门课程，总学分='+R.credit_total+'，GPA='+GPA.util.fix4(R.GPA);
			}
		}
	}
};
