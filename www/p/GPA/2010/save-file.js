
GPA.save.file={
	start:function(){
		GPA.stat('save/file');
		GPA.popup.open('保存为文件','<form id="save_file_form" action="'+GPA.server_base+'save-file.php" method="POST" target="hiddenframe"><div>'
			+'<input type="hidden" id="save_file_data" name="data">'
			+'<table><thead><tr><th>文件格式</th><td>下次可导入</td><td>课程信息</td><td>计算结果</td><td>算法</td><td>分组</td></tr></thead><tbody>'
			+'<tr><th><label><input type="radio" name="format" value="htm" checked>HTML报表(*.htm)</label></th><td>√</td><td>√</td><td>√</td><td>×</td><td>√</td></tr>'
			+'<tr><th><label><input type="radio" name="format" value="csv">逗号分隔表格(*.csv)</label></th><td>√</td><td>√</td><td>×</td><td>×</td><td>×</td></tr>'
			+'</tbody></table>'
			+'<p><input type="submit" value="导出文件"></p>'
			+'</div></form>',{height:250}).show();
			$('#save_file_data').val(GPA.save.file.serializeData());
			$('#save_file_form').submit(GPA.save.file.onSubmitForm);
	},
	serializeData:function(){
		var courses=[],alg,result;
		GPA.courses.each(function(id,C){
			courses.push($.param(C));
		});
		alg='';
		result=GPA.calc.getResult();
		return $.param({
			courses:courses.join('|'),
			alg:alg,
			result:$.param(result)
		});
	},
	onSubmitForm:function(){
		var FORM=$('#save_file_form');
		var format=FORM.find('[name=format]:checked').val();
		GPA.stat('save/file.'+format);
		setTimeout(GPA.popup.close,2000);
	}
};
