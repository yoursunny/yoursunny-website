<?php
$format=$_POST['format'];

parse_str($_POST['data'],$data);
$courses=array();
foreach (explode('|',$data['courses']) as $l_course) {
	parse_str($l_course,$course);
	$courses[]=$course;
}

header('Content-Disposition: attachment; filename=GPA.'.$format);
header('Content-Type: application/octet-stream');

switch ($format) {
	case 'htm':
		include 'save-html.php';
		break;
	case 'csv':
		include 'save-csv.php';
		break;
}
?>
