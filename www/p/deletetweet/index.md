---
http_status: 410
title: DeleteTweet is closed
section:
  title: Projects
  path: /p/
---

DeleteTweet was an IFTTT integration that allows one to delete a mistyped tweet from Twitter via SMS.
It is closed in Apr 2017 due to low usage.
