---
title: AS200690 IPv6-only LowEndASN
section:
  title: Projects
  path: /p/
---

I operate a personal IPv6 network, AS200690, for educational and experimentation purpose.

* [AS200690 on bgp.tools](https://bgp.tools/as/200690)
* [AS200690 on PeeringDB](https://peerwith.me/200690)

NOC contact: <img src="../../m/email.php?n=4&amp;l=1" alt="" style="background:url(../../m/email.php?n=4&amp;l=0);max-width:none;">

<div id="map" style="width: 100%; height: 80vh;"></div>

## Peering Policy

* Physical IXP: direct peering accepted.
* Virtual IXP: routeserver only.

## Sponsors

I'd like to thank the following sponsors for providing free compute and network resources to AS200690:

<div id="sponsors"></div>

* [Cloudie](https://my.cloudie.sh/index.php?rp=/store/toronto-ca-ixpbgp-vps): Toronto, Canada
* [F4 Networks](https://f4.fo): Kansas City, Missouri, USA
* [Tritan Internet](https://tritan.gg/network): IX transit

<script src="map.js"></script>
<script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZkmau2JPqTHVshtguOiFwC1MhW9AGEck&loading=async&callback=initMap&libraries=marker"></script>
<script src="/assets/2017/shuffle-sponsors.js"></script>
