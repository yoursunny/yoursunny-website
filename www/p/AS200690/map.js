const ROUTERS = {
  syd: {
    label: "Sydney, Australia (HostHatch)",
    position: [151.2093, -33.8688],
  },
  coe: {
    label: "Liberty Lake, Washington, USA (Crunchbits)",
    position: [-117.1124, 47.6743],
  },
  mci: {
    label: "Kansas City, Missouri, USA (F4IX - F4 Networks)",
    position: [-94.5786, 39.0997],
  },
  ytz: {
    label: "Toronto, Canada (Ontario Internet Exchange - Cloudie)",
    position: [-79.3832, 43.6532],
  },
  lux: {
    label: "Roost, Luxembourg (BuyVM)",
    position: [6.0753, 49.7866],
  },
  mst: {
    label: "Eygelshoven, Netherlands (ServerFactory)",
    position: [6.0580, 50.8933],
  },
  hkg: {
    label: "Hong Kong, China (SalmonCloud)",
    position: [114.1694, 22.3193],
  },
};

const LINKS = [
  ['syd', 'mci'],
  ['syd', 'hkg'],
  ['coe', 'mci'],
  ['mci', 'ytz'],
  ['ytz', 'mst'],
  ['ytz', 'lux'],
  ['lux', 'mst'],
  ['lux', 'hkg'],
  ['mst', 'hkg'],
];

function toPoint({ position: [lng, lat] }) {
  return { lng, lat };
}

function initMap() {
  const map = new google.maps.Map(document.querySelector("#map"), {
    mapId: "DEMO_MAP_ID",
    center: { lat: 30, lng: 0 },
    zoom: 2,
    mapTypeControl: false,
    streetViewControl: false,
    fullscreenControl: false,
  });

  const parser = new DOMParser();

  for (const router of Object.values(ROUTERS)) {
    const pin = parser.parseFromString(`
      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="-8 -8 16 16" style="transform:translate(0,8px)">
        <path style="transform:scale(8)" d="M 0,1 1,0 0,-1 -1,0 Z" fill="#ff4136" fill-opacity="0.8"/>
      </svg>
    `, "image/svg+xml").documentElement;
    new google.maps.marker.AdvancedMarkerElement({
      map,
      position: toPoint(router),
      title: router.label,
      content: pin,
    });
  }

  for (const [src, dst] of LINKS) {
    new google.maps.Polyline({
      map,
      path: [toPoint(ROUTERS[src]), toPoint(ROUTERS[dst])],
      geodesic: true,
      strokeColor: "#2d4062",
      strokeOpacity: 0.5,
      strokeWeight: 2,
    });
  }
}
