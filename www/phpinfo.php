<?php
header('X-Accel-Expires: 0');
if ($_SERVER['REMOTE_ADDR'] === 'unix:') {
  phpinfo();
  die;
}
http_response_code(402);

$args = array();
$args['title'] = 'phpinfo()';
require_once $_SERVER['DOCUMENT_ROOT'].'/_internal/templating.php';
renderTemplate('+'.__COMPILER_HALT_OFFSET__.':'.__FILE__, $args);
__halt_compiler()?>
{% extends "layout.html" %}

{% block head_extra %}
<meta name="robots" content="noindex,follow">
<style>
.pushup-img { width: 512px; height: 256px; overflow: hidden; }
</style>
{% endblock %}

{% block content %}
<h1>HTTP/2 402 "push-ups required"</h1>
<p>push-ups are required to access <kbd>phpinfo()</kbd>.</p>
<p>Please do push-ups in front of the browser and you will be granted access.</p>

<div class="pushup-img">
<a href="https://pushups.ndn.today">
<img src="https://i.imgur.com/ugIIam5.png" width="512" height="512" alt="yoursunny push-ups logo by @FAT32">
</a>
</div>

{% endblock %}
