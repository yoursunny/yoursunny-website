#!/bin/bash
set -euo pipefail
cd "$(dirname "${BASH_SOURCE[0]}")"/..

rsync -rLtv --delete --delete-excluded --chmod=D770,F660 --no-o --no-g \
  --exclude 'node_modules' --exclude '.git' \
  ./ vps5-php:/home/web/yoursunny2017

echo Bugsnag $(curl -sfLS -F 'apiKey=c2a179e6c7c3ab92efed484ce6b6e411' https://notify.bugsnag.com/deploy)
