#!/bin/bash
# start website in WSL2 container

sudo pkill -x caddy
sudo rm -rf /run/nginx-yoursunny2017.sock /run/shm/nginx-yoursunny2017-cache

sudo service nginx restart

if service php8.3-fpm status; then
  sudo service php8.3-fpm restart
else
  sudo service php8.3-fpm start
fi

sudo /usr/bin/caddy run --config /etc/caddy/Caddyfile &>/tmp/caddy.log &
