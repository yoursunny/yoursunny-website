# HTTP server configuration

## TLS

Caddy handles TLS termination.
Add to `/etc/caddy/Caddyfile`:

```caddy
https://yoursunny.com { # prepend local. for development
  tls internal   # only for development
  bind 127.0.0.1 # only for development
  import /home/web/yoursunny2017/http-server/Caddyfile-body
  respond /robots.txt 404 # delete for development
  log { # only for production
    output file /var/log/caddy/yoursunny2017.log {
      roll_size 32MiB
      roll_keep 5
    }
    format filter {
      wrap json
      fields {
        request>remote_ip delete
        request>remote_port delete
        request>tls delete
        request>host delete
        request>headers delete
        user_id delete
        common_log delete
        resp_headers delete
      }
    }
  }
}
```

## NGINX and PHP

Enable [NGINX repository](https://nginx.org/en/linux_packages.html).

```bash
sudo add-apt-repository ppa:ondrej/php
sudo apt install --no-install-recommends nginx php8.3-bcmath php8.3-cli php8.3-curl php8.3-fpm php8.3-gd php8.3-gmp php8.3-mbstring php8.3-sqlite3 php8.3-xml sqlite3 unzip
sudo adduser nginx www-data

sudo rm /etc/nginx/conf.d/default.conf
sudo ln -s /home/web/yoursunny2017/http-server/nginx-site /etc/nginx/conf.d/site-yoursunny2017.conf

# in WSL only
echo -e 'fastcgi_buffer_size 128k;\nfastcgi_buffers 4 256k;' | sudo tee /etc/nginx/conf.d/wsl_fastcgi_buffer.conf
```

## main site

1. [Download Composer](https://getcomposer.org/download/) and install it `sudo mv composer.phar /usr/local/bin/composer`
2. Install site dependencies `composer install`
3. `cd p/GPA/2010/ && make && cd ../../../`
4. `cd _internal/geoip/ && make MMDB_LICENSE_KEY=xxxxxxxx && cd ../../`
5. `cd p/rideon-today/ && bash fetch.sh && cd ../../`
6. Download [Ruffle](https://github.com/ruffle-rs/ruffle/releases) and extract to `/assets/ruffle` directory.

## /study sub-site

1. Install Ruby `sudo apt install build-essential ruby ruby-dev`
2. Install Gems `sudo gem install bundler jekyll -- --use-system-libraries`, then [update Gems](https://bundler.io/blog/2019/05/14/solutions-for-cant-find-gem-bundler-with-executable-bundle.html) `sudo gem update`
3. Install site dependencies `bundle install`
4. Build site `bundle exec jekyll build --incremental`

## /t sub-site

1. Install Node.js 20.x
2. Install site dependencies `npm install`
3. Build site `npm run watch`
4. `cd demo; composer install`
