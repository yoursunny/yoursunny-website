#!/bin/bash
set -euo pipefail
DOMAIN=https://local.yoursunny.com

GET() {
  curl -sfkI $DOMAIN$1 2>&1 | grep -i '^Location: ' | tr -d '\r' | awk '{print $2}'
}

TEST() {
  local REQUEST=$1
  local EXPECTED=$2
  local ACTUAL=$(GET $1)

  echo -n $REQUEST $EXPECTED
  if [[ $ACTUAL == $EXPECTED ]] || ( [[ $EXPECTED == /* ]] && [[ $ACTUAL == $DOMAIN$EXPECTED ]] ); then
    echo -e " \e[92mOK\e[0m"
  else
    echo -ne " \e[91mBAD\e[0m "
    echo $ACTUAL
  fi
}

TEST /images/10.css '/assets/2010/10.css'
TEST /lib/10.js '/assets/2010/10.js'
TEST /m '/m/'
TEST /m/spirit.htm '/t/2005/sunny-boy-spirit/'
TEST /m/spirit/ '/t/2005/sunny-boy-spirit/'
TEST /m/typo.htm '/t/2009/typo/'
TEST /m/typo/ '/t/2009/typo/'
TEST /study/gpa/ '/p/GPA/'
TEST /study/GPA '/p/GPA/'
TEST /study/GPA/2009 '/p/GPA/'
TEST /study/EI206 '/t/2007/elevator/'
TEST /study/EI209/?topic=index '/study/EI209/'
TEST /study/EI209/?topic=HDD '/study/EI209/HDD.htm'
TEST /study/IS206/?p=nachos-vm '/study/IS206/nachos-vm.pdf'
TEST /study/IS206/nachos1.htm '/study/IS206/nachos-scheduler.pdf'
TEST /study/IS208/?w=r8 '/study/IS209/08.htm'
TEST /study/IS210 '/t/2008/underwater-communication/'
TEST /study/IS215/?p=outline '/study/IS215/outline.htm'
TEST /study/IS216/?question=list '/study/IS216/'
TEST /study/IS216/?question=CSP '/study/IS216/CSP.htm'
TEST /study/IS216/q/ '/study/IS216/'
TEST /study/IS216/q/CSP.htm '/study/IS216/CSP.htm'
TEST /study/IS219/?p=1 '/study/IS219/p1.htm'
TEST /study/IS220/?p=3 '/study/IS220/p3.htm'
TEST /study/IS222/w1.htm '/study/IS222/game-console.pdf'
TEST /study/IS222/?p=linux-arm '/study/IS222/linux-arm.pdf'
TEST /study/IS401/?p=TeamComm '/study/IS401/TeamComm.pdf'
TEST /study/MU019/?p=teaching '/study/MU019/'
TEST /study/MU019/?p=index '/study/MU019/'
TEST /study/MU019/?p=03 '/study/MU019/03.htm'
TEST /study/chemistry/?nb=33 '/study/chemistry/notebook/#p=33'
TEST /study/chemistry/?organic-experiment=index '/study/chemistry/organic-experiment/'
TEST /study/chemistry/?organic-experiment=c2h2 '/study/chemistry/organic-experiment/c2h2.htm'
TEST /p '/p/'
TEST /p/NDNSF/ 'https://github.com/yoursunny/NDNSF'
TEST /p/homecam/ 'https://homecam.ndn.today/'
TEST /p/ndn-cxx-breaks/ 'https://ndn-cxx-breaks.ndn.today/'
TEST /t '/t/'
TEST /work/ '/p/'
TEST /work/AlbumPlayer/ '/t/2007/AlbumPlayer/'
TEST /work/ATM/ '/study/EE305/'
TEST /work/dbMySQL/ '/t/2008/dbMySQL/'
TEST /work/hProxyN/ '/study/IS494/'
TEST /work/ImageResizer/ '/t/2007/ImageResizer/'
TEST /work/PHP-decode/ '/p/PHP-decode/'
